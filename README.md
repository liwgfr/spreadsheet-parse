# README #
------
*Для начала копируем нужную гугл таблицу (Файл - создать копию) и называем ее понятно. Например, 'Copy'*
---------
# Затем идем в Google API Console https://console.developers.google.com/apis/dashboard и создаем свой проект -> Enable APIS and services #
-------
# Там мы ищем Google drive api. Включаем. Настраиваем, как тут: https://s3.amazonaws.com/com.twilio.prod.twilio-docs/original_images/google-developer-console.gif #
-------
*Скачиваем json файл. Дальше кидаем его в директорию нашего main.py (удобнее переименовать на service_account.json) и копируем адрес почты и открываем доступ в нашей скопированной таблице, как тут: https://s3.amazonaws.com/com.twilio.prod.twilio-docs/original_images/share-google-spreadshet.gif*
--------
## Наконец, фиксим путь до нашего service_account.json в main.py и пробуем запустить, предварительно в main.py определив название/key/url нашей скопированной таблицы (docs: https://github.com/burnash/gspread). Пробуем запустить. Скорее всего, выдаст ошибку о SpreadSheet APIS и выдаст ссылку в консоль. Переходим и включаем. Ждем минуту и перезапускаем. Вуаля, все должно работать! ##
-----------